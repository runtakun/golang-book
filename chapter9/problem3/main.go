package main

import ("fmt"; "math")

func distance(x1, x2, y1, y2 float64) float64 {
	a := x2 - x1
	b := y2 - y1
	return math.Sqrt(a * a + b * b)
}

type Shape interface {
	area() float64
	perimeter() float64
}

func totalPerimeter(shapes ...Shape) float64 {
	var area float64
	for _, s := range shapes {
		area += s.perimeter()
	}
	return area
}

type Circle struct {
	x, y, r float64
}

func (c *Circle) area() float64 {
	return math.Pi * c.r * c.r	
}

func (c *Circle) perimeter() float64 {
	return 2.0 * math.Pi * c.r	
}

type Rectangle struct {
	x1, y1, x2, y2 float64
}

func (r *Rectangle) area() float64 {
	l := distance(r.x1, r.y1, r.x1, r.y2)
	w := distance(r.x1, r.y1, r.x2, r.y1)
	return l * w
}

func (r *Rectangle) perimeter() float64 {
	d1 := distance(r.x1, r.y1, r.x1, r.y2)
	d2 := distance(r.x1, r.y2, r.x2, r.y2)
	d3 := distance(r.x2, r.y2, r.x2, r.y1)
	d4 := distance(r.x2, r.y1, r.x1, r.y1)

	return d1 + d2 + d3 + d4
}

func main() {
	c := Circle{0, 0, 5}
	r := Rectangle{0, 0, 10, 10}
	fmt.Println(totalPerimeter(&c, &r))
}
