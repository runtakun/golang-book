package main

import "fmt"

func main() {
	x := make([]int, 5, 10)
	fmt.Println(x)

	slice1 := []int{1,2,3}
	slice2 := append(slice1, 4, 5)
	fmt.Println(slice1, slice2)
}
