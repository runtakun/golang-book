package main

import ("fmt"; "time")

func Sleep(t uint64) {
	<- time.After(time.Millisecond * time.Duration(t))
}

func main() {
	var t uint64
	t = 5 * 1000
	fmt.Println("waiting for", t, "msec")
	Sleep(t)
	fmt.Println("done")
}
