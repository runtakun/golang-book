package main

import "fmt"

func main() {
	var x string = "Hello World"
	fmt.Println(x)

	var y string
	y = "Hello World"
	fmt.Println(y)

	var z string
	z = "first"
	fmt.Println(z)
	z = "second"
	fmt.Println(z)

	var a string = "hello"
	var b string = "hello"
	fmt.Println(a == b)

	c := 5
	fmt.Println(c)

	name := "Max"
	fmt.Println("My dog's name is", name)
}
