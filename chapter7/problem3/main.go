package main

import "fmt"

func max(xs []int) int {
	max := xs[0]
	for _, v := range xs {
		if v > max {
			max = v
		}
	}
	return max
}

func main() {
	xs := []int{1000,2,3, 99}
	fmt.Println(max(xs))
}
