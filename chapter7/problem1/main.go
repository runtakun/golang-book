package main

import "fmt"

func sum(xs ...int) int {
	total := 0
	for _, v := range xs {
		total += v
	}
	return total
}

func main() {
	fmt.Println(sum(1, 3, 5))
}
